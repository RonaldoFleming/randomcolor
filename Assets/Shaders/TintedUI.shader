﻿Shader "Custom/TintedUI" {
	Properties {
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
	}
	SubShader {
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0

			sampler2D _MainTex;

			struct VertexInput
			{
				fixed4 position : POSITION;
				float4 color    : COLOR;
				fixed2 uv : TEXCOORD0;
			};

			struct VertexOutput
			{
				fixed4 position : SV_POSITION;
				fixed4 color : COLOR;
				fixed2 uv : TEXCOORD0;
			};

			VertexOutput vert(VertexInput input)
			{
				VertexOutput output;
				output.position = UnityObjectToClipPos(input.position);
				output.uv = input.uv;
				output.color = input.color;

				return output;
			}

			fixed4 frag(VertexOutput input) : COLOR
			{
				fixed4 fragmentColor = tex2D(_MainTex, input.uv) * input.color;

				return fragmentColor;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}