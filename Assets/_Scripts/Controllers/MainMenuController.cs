﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> _popUpsList;
    [SerializeField]
    private GameObject _quitPanel;

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            ShowQuitPanel();
        }
    }

    private void HideAllPopUps()
    {
        foreach(GameObject popUp in _popUpsList)
        {
            if(popUp.activeSelf)
            {
                popUp.SetActive(false);
            }
        }
    }

    public void Begin()
    {
        Observer.OnButtonTouched(this);
        Observer.OnBeginButtonTouched(this);
    }

    public void QuitGame()
    {
        Observer.OnButtonTouched(this);
        Application.Quit();
    }

    public void CancelQuit()
    {
        Observer.OnButtonTouched(this);
        _quitPanel.SetActive(false);
    }

    private void ShowQuitPanel()
    {
        HideAllPopUps();
        _quitPanel.SetActive(true);
    }
}
