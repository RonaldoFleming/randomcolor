﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RandomColor;
using Bitcraft.StateMachine;

public class RandomColorCanvasController : MonoBehaviour
{
    public List<Color> _colorsList;
    public Image _colorImage;
    public float _delayUntilNextColor = 3f;
    public GameObject _invalidColorPanel;

    private RandomColorFSMStateMachine _randomColorFSM;

    #region Data Holders
    public class ColorValidationData
    {
        public readonly Color OldColor;
        public readonly Color NewColor;

        public ColorValidationData(Color oldColor, Color newColor)
        {
            OldColor = oldColor;
            NewColor = newColor;
        }
    }

    public class DisplayColorData
    {
        public readonly Image ColorImage;
        public readonly Color ColorToDisplay;

        public DisplayColorData(Image colorImage, Color colorToDisplay)
        {
            ColorImage = colorImage;
            ColorToDisplay = colorToDisplay;
        }
    }
    #endregion

    private void Start()
    {
        Observer.ColorGenerated += ColorGenerated;
        Observer.InvalidColor += InvalidColor;
        Observer.ValidColor += ValidColor;
        Observer.ColorDisplayed += ColorDisplayed;

        _randomColorFSM = new RandomColorFSMStateMachine();
        _randomColorFSM.SetInitialState(RandomColorFSMStateTokens.GenerateColor, _colorsList);
    }

    private void OnDestroy()
    {
        Observer.ColorGenerated -= ColorGenerated;
        Observer.InvalidColor -= InvalidColor;
        Observer.ValidColor -= ValidColor;
        Observer.ColorDisplayed -= ColorDisplayed;
    }

    private void ColorGenerated(object sender, System.EventArgs e)
    {
        Observer.ColorGeneratedEventArgs args = e as Observer.ColorGeneratedEventArgs;

        StartCoroutine(PerformAction(RandomColorFSMActionTokens.Next, new ColorValidationData(_colorImage.color, args.GeneratedColor)));
    }

    private void InvalidColor(object sender, System.EventArgs e)
    {
        _invalidColorPanel.SetActive(true);
    }

    private void ValidColor(object sender, System.EventArgs e)
    {
        Observer.ValidColorEventArgs args = e as Observer.ValidColorEventArgs;
        StartCoroutine(PerformAction(RandomColorFSMActionTokens.Valid, new DisplayColorData(_colorImage, args.ValidatedColor)));
    }

    private void ColorDisplayed(object sender, System.EventArgs e)
    {
        StartCoroutine(GenerateNextColor(_delayUntilNextColor));
    }

    private IEnumerator PerformAction(ActionToken token, object data)
    {
        yield return new WaitForEndOfFrame();

        _randomColorFSM.PerformAction(token, data);
    }

    private IEnumerator GenerateNextColor(float delay)
    {
        yield return new WaitForSeconds(delay);
        StartCoroutine(PerformAction(RandomColorFSMActionTokens.Next, _colorsList));
    }

    public void Retry()
    {
        Observer.OnButtonTouched(this);
        Observer.OnRetryButtonTouched(this);
        _invalidColorPanel.SetActive(false);
        StartCoroutine(PerformAction(RandomColorFSMActionTokens.Invalid, _colorsList));
    }

    public void Quit()
    {
        Observer.OnButtonTouched(this);
        Observer.OnQuitRandomColorButtonTouched(this);
    }
}
