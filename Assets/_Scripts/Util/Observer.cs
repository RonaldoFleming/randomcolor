﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{

    #region Random Color events

    #region Color Generated

    public class ColorGeneratedEventArgs : EventArgs
    {
        public readonly Color GeneratedColor;

        public ColorGeneratedEventArgs(Color generatedColor)
        {
            GeneratedColor = generatedColor;
        }
    }

    public static event EventHandler ColorGenerated;

    public static void OnColorGenerated(object sender, Color generatedColor)
    {
        if (ColorGenerated != null)
        {
            ColorGenerated(sender, new ColorGeneratedEventArgs(generatedColor));
        }
    }

    #endregion

    #region Invalid Color

    public static event EventHandler InvalidColor;

    public static void OnInvalidColor(object sender)
    {
        if (InvalidColor != null)
        {
            InvalidColor(sender, EventArgs.Empty);
        }
    }

    #endregion

    #region Valid Color

    public class ValidColorEventArgs : EventArgs
    {
        public readonly Color ValidatedColor;

        public ValidColorEventArgs(Color validatedColor)
        {
            ValidatedColor = validatedColor;
        }
    }

    public static event EventHandler ValidColor;

    public static void OnValidColor(object sender, Color validatedColor)
    {
        if (ValidColor != null)
        {
            ValidColor(sender, new ValidColorEventArgs(validatedColor));
        }
    }

    #endregion

    #region Color Displayed

    public static event EventHandler ColorDisplayed;

    public static void OnColorDisplayed(object sender)
    {
        if (ColorDisplayed != null)
        {
            ColorDisplayed(sender, EventArgs.Empty);
        }
    }

    #endregion

    #endregion

    #region GUI events

    #region Button Touched

    public static event EventHandler ButtonTouched;

    public static void OnButtonTouched(object sender)
    {
        if (ButtonTouched != null)
        {
            ButtonTouched(sender, EventArgs.Empty);
        }
    }

    #endregion

    #region Begin Button Touched

    public static event EventHandler BeginButtonTouched;

    public static void OnBeginButtonTouched(object sender)
    {
        if (BeginButtonTouched != null)
        {
            BeginButtonTouched(sender, EventArgs.Empty);
        }
    }

    #endregion

    #region Quit Random Color Button Touched

    public static event EventHandler QuitRandomColorButtonTouched;

    public static void OnQuitRandomColorButtonTouched(object sender)
    {
        if (QuitRandomColorButtonTouched != null)
        {
            QuitRandomColorButtonTouched(sender, EventArgs.Empty);
        }
    }

    #endregion

    #region Retry Button Touched

    public static event EventHandler RetryButtonTouched;

    public static void OnRetryButtonTouched(object sender)
    {
        if (RetryButtonTouched != null)
        {
            RetryButtonTouched(sender, EventArgs.Empty);
        }
    }

    #endregion

    #region Random Color Loading Shown

    public static event EventHandler RandomColorLoadingShown;

    public static void OnRandomColorLoadingShown(object sender)
    {
        if (RandomColorLoadingShown != null)
        {
            RandomColorLoadingShown(sender, EventArgs.Empty);
        }
    }

    #endregion

    #region Random Color Loading Done

    public static event EventHandler RandomColorLoadingDone;

    public static void OnRandomColorLoadingDone(object sender)
    {
        if (RandomColorLoadingDone != null)
        {
            RandomColorLoadingDone(sender, EventArgs.Empty);
        }
    }

    #endregion

    #region Main Menu Loading Shown

    public static event EventHandler MainMenuLoadingShown;

    public static void OnMainMenuLoadingShown(object sender)
    {
        if (MainMenuLoadingShown != null)
        {
            MainMenuLoadingShown(sender, EventArgs.Empty);
        }
    }

    #endregion

    #endregion

    #region Scene Management events

    #region Random Color Scene Loaded

    public static event EventHandler RandomColorSceneLoaded;

    public static void OnRandomColorSceneLoaded(object sender)
    {
        if (RandomColorSceneLoaded != null)
        {
            RandomColorSceneLoaded(sender, EventArgs.Empty);
        }
    }

    #endregion

    #region Main Menu Scene Loaded

    public static event EventHandler MainMenuSceneLoaded;

    public static void OnMainMenuSceneLoaded(object sender)
    {
        if (MainMenuSceneLoaded != null)
        {
            MainMenuSceneLoaded(sender, EventArgs.Empty);
        }
    }

    #endregion

    #endregion

}