﻿using UnityEngine;

public class SoundManager : SingletonMonoBehaviour<SoundManager>
{
    public AudioClip _buttonClip;
    public AudioClip _newColorClip;
    public AudioClip _errorClip;

    private AudioSource _source;

    void Start ()
    {
        Observer.ButtonTouched += ButtonTouched;
        Observer.ColorDisplayed += ColorDisplayed;
        Observer.InvalidColor += InvalidColor;

        _source = gameObject.GetComponent<AudioSource>();
	}

    private void OnDestroy()
    {
        Observer.ButtonTouched -= ButtonTouched;
        Observer.ColorDisplayed -= ColorDisplayed;
        Observer.InvalidColor -= InvalidColor;
    }

    private void ButtonTouched(object sender, System.EventArgs e)
    {
        _source.PlayOneShot(_buttonClip);
    }

    private void ColorDisplayed(object sender, System.EventArgs e)
    {
        _source.PlayOneShot(_newColorClip);
    }

    private void InvalidColor(object sender, System.EventArgs e)
    {
        _source.PlayOneShot(_errorClip);
    }
}
