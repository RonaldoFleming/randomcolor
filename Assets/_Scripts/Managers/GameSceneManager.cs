﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : SingletonMonoBehaviour<GameSceneManager>
{
    private void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        Observer.RandomColorLoadingShown += RandomColorLoadingShown;
        Observer.MainMenuLoadingShown += MainMenuLoadingShown;
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        Observer.RandomColorLoadingShown -= RandomColorLoadingShown;
        Observer.MainMenuLoadingShown -= MainMenuLoadingShown;
    }

    private void RandomColorLoadingShown(object sender, EventArgs e)
    {
        SceneManager.LoadSceneAsync("RandomColor");
    }

    private void MainMenuLoadingShown(object sender, EventArgs e)
    {
        SceneManager.LoadSceneAsync("MainMenu");
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == "RandomColor")
        {
            Observer.OnRandomColorSceneLoaded(this);
        }
        else if(scene.name == "MainMenu")
        {
            Observer.OnMainMenuSceneLoaded(this);
        }
    }
}
