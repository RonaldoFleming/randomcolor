﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreenManager : SingletonMonoBehaviour<LoadingScreenManager>
{
    [SerializeField]
    private Animator _loadingPanelAnimator;
    [SerializeField]
    private AnimationClip _fadeClip;

    private LoadingType _currentLoadingType;

    public enum LoadingType
    {
        RandomColor,
        MainMenu
    }

    private void Start()
    {
        Observer.BeginButtonTouched += BeginButtonTouched;
        Observer.RandomColorSceneLoaded += RandomColorSceneLoaded;
        Observer.QuitRandomColorButtonTouched += QuitRandomColorButtonTouched;
        Observer.MainMenuSceneLoaded += MainMenuSceneLoaded;
    }

    private void OnDestroy()
    {
        Observer.BeginButtonTouched -= BeginButtonTouched;
        Observer.RandomColorSceneLoaded -= RandomColorSceneLoaded;
        Observer.QuitRandomColorButtonTouched -= QuitRandomColorButtonTouched;
        Observer.MainMenuSceneLoaded -= MainMenuSceneLoaded;
    }

    private void BeginButtonTouched(object sender, System.EventArgs e)
    {
        _currentLoadingType = LoadingType.RandomColor;
        StartCoroutine(ShowLoading());
    }

    private void RandomColorSceneLoaded(object sender, System.EventArgs e)
    {
        StartCoroutine(HideLoading());
    }

    private void QuitRandomColorButtonTouched(object sender, EventArgs e)
    {
        _currentLoadingType = LoadingType.MainMenu;
        StartCoroutine(ShowLoading());
    }

    private void MainMenuSceneLoaded(object sender, EventArgs e)
    {
        StartCoroutine(HideLoading());
    }

    public float FadeTime {
        get { return _fadeClip.length; }
    }

    private IEnumerator ShowLoading()
    {
        _loadingPanelAnimator.gameObject.SetActive(true);
        _loadingPanelAnimator.SetTrigger("FadeIn");

        yield return new WaitForSeconds(FadeTime);

        if(_currentLoadingType == LoadingType.RandomColor)
        {
            Observer.OnRandomColorLoadingShown(this);
        }
        else if(_currentLoadingType == LoadingType.MainMenu)
        {
            Observer.OnMainMenuLoadingShown(this);
        }

    }

    private IEnumerator HideLoading()
    {
        _loadingPanelAnimator.SetTrigger("FadeOut");

        yield return new WaitForSeconds(FadeTime);

        _loadingPanelAnimator.gameObject.SetActive(false);

        if(_currentLoadingType == LoadingType.RandomColor)
        {
            Observer.OnRandomColorLoadingDone(this);
        }
    }
}
