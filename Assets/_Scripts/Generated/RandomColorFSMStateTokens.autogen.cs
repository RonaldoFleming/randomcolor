/*
 ###########################################################
 ### Warning: this file has been generated automatically ###
 ###                    DO NOT MODIFY                    ###
 ###########################################################
*/

using System;
using Bitcraft.StateMachine;

namespace RandomColor
{
    public static class RandomColorFSMStateTokens
    {
        public static readonly StateToken GenerateColor = new StateToken("GenerateColor");
        public static readonly StateToken ValidadeColor = new StateToken("ValidadeColor");
        public static readonly StateToken DisplayColor = new StateToken("DisplayColor");

        public static readonly StateToken[] Items = 
        {
            GenerateColor,
            ValidadeColor,
            DisplayColor
        };
    }
}
