/*
 ###########################################################
 ### Warning: this file has been generated automatically ###
 ###                    DO NOT MODIFY                    ###
 ###########################################################
*/

using System;
using System.Collections.Generic;
using Bitcraft.StateMachine;
using UnityEngine;

namespace RandomColor.States
{
    partial class RandomColorFSMGenerateColorState : StateBase
    {
        public RandomColorFSMGenerateColorState()
            : base(RandomColorFSMStateTokens.GenerateColor)
        {
        }

        protected override void OnInitialized()
        {
            PreInitialized();

            base.OnInitialized();
            RegisterActionHandler(RandomColorFSMActionTokens.Next, OnRandomColorFSMNextAction);

            PostInitialized();
        }

        partial void PreInitialized();
        partial void PostInitialized();

        protected override void OnEnter(StateEnterEventArgs e)
        {
            List<Color> colorsList = e.Data as List<Color>;
            Observer.OnColorGenerated(this, colorsList[UnityEngine.Random.Range(0, colorsList.Count)]);
        }

        private void OnRandomColorFSMNextAction(object data, Action<StateToken> callback)
        {
            callback(RandomColorFSMStateTokens.ValidadeColor);
        }
    }
}
