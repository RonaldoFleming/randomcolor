/*
 ###########################################################
 ### Warning: this file has been generated automatically ###
 ###                    DO NOT MODIFY                    ###
 ###########################################################
*/

using System;
using Bitcraft.StateMachine;

namespace RandomColor.States
{
    partial class RandomColorFSMValidadeColorState : StateBase
    {
        public RandomColorFSMValidadeColorState()
            : base(RandomColorFSMStateTokens.ValidadeColor)
        {
        }

        protected override void OnInitialized()
        {
            PreInitialized();

            base.OnInitialized();
            RegisterActionHandler(RandomColorFSMActionTokens.Valid, OnRandomColorFSMValidAction);
            RegisterActionHandler(RandomColorFSMActionTokens.Invalid, OnRandomColorFSMInvalidAction);

            PostInitialized();
        }

        partial void PreInitialized();
        partial void PostInitialized();

        protected override void OnEnter(StateEnterEventArgs e)
        {
            RandomColorCanvasController.ColorValidationData data = e.Data as RandomColorCanvasController.ColorValidationData;

            if(data.OldColor == data.NewColor)
            {
                Observer.OnInvalidColor(this);
            }
            else
            {
                Observer.OnValidColor(this, data.NewColor);
            }
        }

        private void OnRandomColorFSMValidAction(object data, Action<StateToken> callback)
        {
            callback(RandomColorFSMStateTokens.DisplayColor);
        }

        private void OnRandomColorFSMInvalidAction(object data, Action<StateToken> callback)
        {
            callback(RandomColorFSMStateTokens.GenerateColor);
        }
    }
}
