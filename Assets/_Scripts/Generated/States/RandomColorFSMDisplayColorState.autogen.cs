/*
 ###########################################################
 ### Warning: this file has been generated automatically ###
 ###                    DO NOT MODIFY                    ###
 ###########################################################
*/

using System;
using Bitcraft.StateMachine;

namespace RandomColor.States
{
    partial class RandomColorFSMDisplayColorState : StateBase
    {
        public RandomColorFSMDisplayColorState()
            : base(RandomColorFSMStateTokens.DisplayColor)
        {
        }

        protected override void OnInitialized()
        {
            PreInitialized();

            base.OnInitialized();
            RegisterActionHandler(RandomColorFSMActionTokens.Next, OnRandomColorFSMNextAction);

            PostInitialized();
        }

        partial void PreInitialized();
        partial void PostInitialized();

        protected override void OnEnter(StateEnterEventArgs e)
        {
            RandomColorCanvasController.DisplayColorData data = e.Data as RandomColorCanvasController.DisplayColorData;

            data.ColorImage.color = data.ColorToDisplay;

            Observer.OnColorDisplayed(this);
        }

        private void OnRandomColorFSMNextAction(object data, Action<StateToken> callback)
        {
            callback(RandomColorFSMStateTokens.GenerateColor);
        }
    }
}
