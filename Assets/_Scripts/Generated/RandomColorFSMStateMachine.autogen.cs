/*
 ###########################################################
 ### Warning: this file has been generated automatically ###
 ###                    DO NOT MODIFY                    ###
 ###########################################################
*/

using System;
using Bitcraft.StateMachine;
using RandomColor.States;

namespace RandomColor
{
    partial class RandomColorFSMStateMachine : StateManager
    {
        public RandomColorFSMStateMachine()
            : this(null)
        {
        }

        public RandomColorFSMStateMachine(object context)
            : base(context)
        {
            PreHandlersRegistration();

            RegisterState(new RandomColorFSMGenerateColorState());
            RegisterState(new RandomColorFSMValidadeColorState());
            RegisterState(new RandomColorFSMDisplayColorState());

            PostHandlersRegistration();
        }

        partial void PreHandlersRegistration();
        partial void PostHandlersRegistration();
    }
}
