/*
 ###########################################################
 ### Warning: this file has been generated automatically ###
 ###                    DO NOT MODIFY                    ###
 ###########################################################
*/

using System;
using Bitcraft.StateMachine;

namespace RandomColor
{
    public static class RandomColorFSMActionTokens
    {
        public static readonly ActionToken Next = new ActionToken("Next");
        public static readonly ActionToken Valid = new ActionToken("Valid");
        public static readonly ActionToken Invalid = new ActionToken("Invalid");

        public static readonly ActionToken[] Items = 
        {
            Next,
            Valid,
            Invalid
        };
    }
}
